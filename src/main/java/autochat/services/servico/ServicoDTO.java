package autochat.services.servico;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class ServicoDTO {

	private Long id;
	
	@NotBlank(message = "É necessário informar o nome do serviço")
	private String nome;
	
	@NotNull(message = "É necessário informar o tempo de atendimento serviço")
	private Integer tempoAtendimento;
	
	@NotNull(message = "É necessário informar o prestador do serviço")
	private Long facebookIdPrestador;
	
	public Long getFacebookIdPrestador() {
		return facebookIdPrestador;
	}

	public void setFacebookIdPrestador(Long prestador) {
		this.facebookIdPrestador = prestador;
	}

	public void setTempoAtendimento(Integer tempoAtendimento) {
		this.tempoAtendimento = tempoAtendimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getTempoAtendimento() {
		return tempoAtendimento;
	}

	public void setTempoAtendimento(int tempoAtendimento) {
		this.tempoAtendimento = tempoAtendimento;
	}
}
