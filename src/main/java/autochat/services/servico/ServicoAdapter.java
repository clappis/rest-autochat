package autochat.services.servico;

import static autochat.models.servico.Servico.getBuilder;

import java.util.ArrayList;
import java.util.List;

import autochat.models.prestador.Prestador;
import autochat.models.servico.Servico;

public class ServicoAdapter {

	Servico adapt(ServicoDTO dto, Prestador prestador){
		if (prestador == null)
			throw new RuntimeException("Não foi possível encontrar o prestador");
		
		return getBuilder()
			.nome(dto.getNome())
			.tempoAtendimento(dto.getTempoAtendimento())
			.prestador(prestador)
			.build();
	}
	
	public ServicoDTO adapt(Servico servico){
		ServicoDTO dto = new ServicoDTO();
		dto.setNome(servico.getNome());
		dto.setId(servico.getId());
		dto.setTempoAtendimento(servico.getTempoAtendimento());
		dto.setFacebookIdPrestador(servico.getPrestador().getFacebookId());
		return dto;
	}

	List<ServicoDTO> adapt(List<Servico> servicos) {
		List<ServicoDTO> dtos = new ArrayList<>();
		for (Servico servico : servicos) 
			dtos.add(adapt(servico));
		
		return dtos;
	}
}
