package autochat.services.servico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import autochat.models.prestador.Prestador;
import autochat.models.prestador.PrestadorRepositorio;
import autochat.models.servico.Servico;
import autochat.models.servico.ServicoRepositorio;

@Service
public class ServicoService {

	private ServicoAdapter adapter = new ServicoAdapter();
	
	@Autowired
	private ServicoRepositorio repositorioServico;
	
	@Autowired
	private PrestadorRepositorio repositorioPrestador;
	
	private PrestadorRepositorio getRepositorioPrestador() {
		return repositorioPrestador;
	}
	
	protected ServicoRepositorio getRepositorioServico() {
		return repositorioServico;
	}

	public List<ServicoDTO> consulta(){
		return adapter.adapt(getRepositorioServico().findAll());
	}
	
	public void inserir(ServicoDTO dto){
		Prestador prestador = getRepositorioPrestador().findByFacebookId(dto.getFacebookIdPrestador());
		Servico servico = adapter.adapt(dto, prestador);
		getRepositorioServico().save(servico);
	}
	
	public ServicoDTO consulta(Long id){
		return adapter.adapt(getRepositorioServico().findById(id));
	}

	public void deletar(Long id) {
		getRepositorioServico().delete(id);
		//TODO: regras afk
	}
	
}
