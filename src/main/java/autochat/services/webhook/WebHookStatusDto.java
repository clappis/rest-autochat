package autochat.services.webhook;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class WebHookStatusDto {

	private int code;
	private String errorDetails;
	private String errorType;
	
	int getCode() {
		return code;
	}
	void setCode(int code) {
		this.code = code;
	}
	String getErrorDetails() {
		return errorDetails;
	}
	void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}
	String getErrorType() {
		return errorType;
	}
	void setErrorType(String errorType) {
		this.errorType = errorType;
	}
}
