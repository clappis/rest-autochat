package autochat.services.webhook.executor;

import autochat.controllers.mensagem.СonfiguracaoMensagem;
import autochat.services.webhook.request.WebhookRequestDto;
import autochat.services.webhook.response.WebhookResponseDto;

public class RequestExecutorBoasVindas extends RequestExecutor {

	static final String BOAS_VINDAS = "BOAS_VINDAS";

	RequestExecutorBoasVindas(WebhookRequestDto request) {
		super(request);
	}

	@Override
	public WebhookResponseDto execute() {
		СonfiguracaoMensagem configuracao = pesquisaConfiguracaoMensagem();
		WebhookResponseDto response = new WebhookResponseDto();
		response.setSpeech(configuracao.getMensagem());
		response.setSource(configuracao.getMensagem());
		return response;
	}

	@Override
	protected String getCodigo() {
		return BOAS_VINDAS;
	}
}
