package autochat.services.webhook.executor;

import static java.lang.Long.valueOf;

import org.springframework.beans.factory.annotation.Autowired;

import autochat.controllers.mensagem.ConfiguracaoMensagemRepositorio;
import autochat.controllers.mensagem.СonfiguracaoMensagem;
import autochat.models.prestador.Prestador;
import autochat.models.prestador.PrestadorRepositorio;
import autochat.services.webhook.request.WebhookRequestDto;
import autochat.services.webhook.response.WebhookResponseDto;

public abstract class RequestExecutor {

	@Autowired
	private ConfiguracaoMensagemRepositorio configuracaoMensagemRepositorio;
	
	@Autowired
	private PrestadorRepositorio prestadorRepositorio;
	
	private WebhookRequestDto request;
	

	RequestExecutor(WebhookRequestDto request){
		this.request = request;
	}
	
	public abstract WebhookResponseDto execute();
	
	protected abstract String getCodigo();

	protected СonfiguracaoMensagem pesquisaConfiguracaoMensagem() {
		String facebookId = getRequest().getOriginalRequest().getData().getSender().getId();
		Prestador prestador = getPrestadorRepositorio().findByFacebookId(valueOf(facebookId));
		СonfiguracaoMensagem configuracao = getConfiguracaoMensagemRepositorio().findByPrestador(prestador, getCodigo());
		return configuracao;
	}
	
	public WebhookRequestDto getRequest() {
		return request;
	}

	public void setRequest(WebhookRequestDto request) {
		this.request = request;
	}

	protected ConfiguracaoMensagemRepositorio getConfiguracaoMensagemRepositorio() {
		return configuracaoMensagemRepositorio;
	}

	protected PrestadorRepositorio getPrestadorRepositorio() {
		return prestadorRepositorio;
	}
}
