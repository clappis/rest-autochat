package autochat.services.webhook.executor;

import static autochat.services.webhook.executor.RequestExecutorBoasVindas.BOAS_VINDAS;

import autochat.services.webhook.request.WebhookRequestDto;

public class ResquestExecutorFactory {

	public static RequestExecutor getInstance(WebhookRequestDto requestDto) {
		String intentName = requestDto.getResult().getMetadata().getIntentName().toUpperCase();
		
		if (BOAS_VINDAS.equals(intentName))
			return new RequestExecutorBoasVindas(requestDto);
		
		return new RequestExecutorFallback(requestDto);
	}
}
