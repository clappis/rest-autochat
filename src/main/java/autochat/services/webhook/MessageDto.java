package autochat.services.webhook;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class MessageDto {

	private String speech;
	private int type;
	private String platform;
	private String imagemUrl;
	
	public String getSpeech() {
		return speech;
	}
	public void setSpeech(String speech) {
		this.speech = speech;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getImagemUrl() {
		return imagemUrl;
	}
	public void setImagemUrl(String imagemUrl) {
		this.imagemUrl = imagemUrl;
	}
	
	
	
}
