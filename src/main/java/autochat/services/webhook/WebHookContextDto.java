package autochat.services.webhook;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class WebHookContextDto {

	private Long lifespan;
	private String name;
	private ParametersDto parameters;
	
	public Long getLifespan() {
		return lifespan;
	}
	public void setLifespan(Long lifespan) {
		this.lifespan = lifespan;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ParametersDto getParameters() {
		return parameters;
	}
	public void setParameters(ParametersDto parameters) {
		this.parameters = parameters;
	}
}
