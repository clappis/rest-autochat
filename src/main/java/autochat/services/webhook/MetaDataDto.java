package autochat.services.webhook;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class MetaDataDto {

	private String intentId;
	private String intentName;
	private String webhookForSlotFillingUsed;
	private Long webhookResponseTime;
	private String webhookUsed;
	
	public String getIntentId() {
		return intentId;
	}
	public void setIntentId(String intentId) {
		this.intentId = intentId;
	}
	public String getIntentName() {
		return intentName;
	}
	public void setIntentName(String intentName) {
		this.intentName = intentName;
	}
	public String getWebhookForSlotFillingUsed() {
		return webhookForSlotFillingUsed;
	}
	public void setWebhookForSlotFillingUsed(String webhookForSlotFillingUsed) {
		this.webhookForSlotFillingUsed = webhookForSlotFillingUsed;
	}
	public Long getWebhookResponseTime() {
		return webhookResponseTime;
	}
	public void setWebhookResponseTime(Long webhookResponseTime) {
		this.webhookResponseTime = webhookResponseTime;
	}
	public String getWebhookUsed() {
		return webhookUsed;
	}
	public void setWebhookUsed(String webhookUsed) {
		this.webhookUsed = webhookUsed;
	}

	
}
