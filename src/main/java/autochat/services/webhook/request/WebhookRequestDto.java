package autochat.services.webhook.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import autochat.services.webhook.WebHookStatusDto;

@JsonSerialize
public class WebhookRequestDto {

	private OriginalRequestDto originalRequest;
	private String id;
	private String lang;
	private WebHookResultDto result;
	private String sessionId;
	private WebHookStatusDto status;
	private String timestamp;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public WebHookResultDto getResult() {
		return result;
	}
	public void setResult(WebHookResultDto result) {
		this.result = result;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public WebHookStatusDto getStatus() {
		return status;
	}
	public void setStatus(WebHookStatusDto status) {
		this.status = status;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public OriginalRequestDto getOriginalRequest() {
		return originalRequest;
	}
	public void setOriginalRequest(OriginalRequestDto originalRequest) {
		this.originalRequest = originalRequest;
	}
}
