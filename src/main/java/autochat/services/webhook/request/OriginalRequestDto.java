package autochat.services.webhook.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class OriginalRequestDto {

	private String source;
	private OriginalRequestDataDto data;
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public OriginalRequestDataDto getData() {
		return data;
	}
	public void setData(OriginalRequestDataDto data) {
		this.data = data;
	}
}
