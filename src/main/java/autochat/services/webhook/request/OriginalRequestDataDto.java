package autochat.services.webhook.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class OriginalRequestDataDto {

	private SenderDto sender;
	private RecipientDto recipient;
	private DataMessageDto message;
	private String timestamp;
	
	public SenderDto getSender() {
		return sender;
	}
	public void setSender(SenderDto sender) {
		this.sender = sender;
	}
	public RecipientDto getRecipient() {
		return recipient;
	}
	public void setRecipient(RecipientDto recipient) {
		this.recipient = recipient;
	}
	public DataMessageDto getMessage() {
		return message;
	}
	public void setMessage(DataMessageDto message) {
		this.message = message;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
}
