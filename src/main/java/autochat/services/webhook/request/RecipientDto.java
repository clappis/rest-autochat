package autochat.services.webhook.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class RecipientDto extends CommunicatorDto {}
