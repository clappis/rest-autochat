package autochat.services.webhook.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class CommunicatorDto {

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
