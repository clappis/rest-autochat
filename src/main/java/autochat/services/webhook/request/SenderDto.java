package autochat.services.webhook.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class SenderDto extends CommunicatorDto {}
