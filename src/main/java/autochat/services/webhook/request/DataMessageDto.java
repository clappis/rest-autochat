package autochat.services.webhook.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class DataMessageDto {

	private String mid;
	private String text;
	private String seq;
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	
	
}
