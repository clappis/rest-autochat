package autochat.services.webhook.request;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import autochat.services.webhook.FulfillmentDto;
import autochat.services.webhook.MetaDataDto;
import autochat.services.webhook.ParametersDto;
import autochat.services.webhook.WebHookContextDto;

@JsonSerialize
public class WebHookResultDto {

	private String action;
	private boolean actionIncomplete;
	private List<WebHookContextDto> contexts;
	private FulfillmentDto fulfillment;
	private MetaDataDto metadata;
	private ParametersDto parameters;
	private String resolvedQuery;
	private int score;
	private String source;
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public boolean isActionIncomplete() {
		return actionIncomplete;
	}
	public void setActionIncomplete(boolean actionIncomplete) {
		this.actionIncomplete = actionIncomplete;
	}
	public List<WebHookContextDto> getContexts() {
		return contexts;
	}
	public void setContexts(List<WebHookContextDto> contexts) {
		this.contexts = contexts;
	}
	public FulfillmentDto getFulfillment() {
		return fulfillment;
	}
	public void setFulfillment(FulfillmentDto fulfillment) {
		this.fulfillment = fulfillment;
	}
	public MetaDataDto getMetadata() {
		return metadata;
	}
	public void setMetadata(MetaDataDto metadata) {
		this.metadata = metadata;
	}
	public ParametersDto getParameters() {
		return parameters;
	}
	public void setParameters(ParametersDto parameters) {
		this.parameters = parameters;
	}
	public String getResolvedQuery() {
		return resolvedQuery;
	}
	public void setResolvedQuery(String resolvedQuery) {
		this.resolvedQuery = resolvedQuery;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
		
}
