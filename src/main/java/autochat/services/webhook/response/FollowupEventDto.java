package autochat.services.webhook.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class FollowupEventDto {

	private String name;
	private String data;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
}
