package autochat.services.webhook.response;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import autochat.services.webhook.WebHookContextDto;

@JsonSerialize
public class WebhookResponseDto {

	private String speech;
	private String displayText;
	private WebHookResponseData data;
	private List<WebHookContextDto> contextOut;
	private String source;
	private FollowupEventDto followupEvent;
	
	public String getSpeech() {
		return speech;
	}
	public void setSpeech(String speech) {
		this.speech = speech;
	}
	public String getDisplayText() {
		return displayText;
	}
	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}
	public WebHookResponseData getData() {
		return data;
	}
	public void setData(WebHookResponseData data) {
		this.data = data;
	}
	public List<WebHookContextDto> getContextOut() {
		return contextOut;
	}
	public void setContextOut(List<WebHookContextDto> contextOut) {
		this.contextOut = contextOut;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public FollowupEventDto getFollowupEvent() {
		return followupEvent;
	}
	public void setFollowupEvent(FollowupEventDto followupEvent) {
		this.followupEvent = followupEvent;
	}
}
