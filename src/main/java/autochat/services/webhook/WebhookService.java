package autochat.services.webhook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import autochat.models.webhook.WebhookHistory;
import autochat.models.webhook.WebhookHistoryRepositorio;
import autochat.services.webhook.executor.RequestExecutor;
import autochat.services.webhook.executor.ResquestExecutorFactory;
import autochat.services.webhook.request.WebhookRequestDto;
import autochat.services.webhook.response.WebhookResponseDto;

@Service
public class WebhookService {

	@Autowired
	private WebhookHistoryRepositorio repositorioWebhook;
	
	private final static Logger logger = LoggerFactory.getLogger(WebhookService.class);
	
	public WebhookResponseDto processaRequest(String request){
		WebhookHistory webHookHistory = historizaRequest(request);
		
		WebhookRequestDto requestDto = parseRequest(request);
		WebhookResponseDto responseDto = processaRequest(requestDto);
		
		historizaResponse(webHookHistory, responseDto);
		return responseDto;
	}

	private void historizaResponse(WebhookHistory webHookHistory, WebhookResponseDto responseDto) {
		try {
			String response = new Gson().toJson(responseDto);
			webHookHistory.setResponse(response);
			repositorioWebhook.save(webHookHistory);
		} catch (Exception e) {
			logger.info(e.getMessage(), e.getCause());
		}
	}

	private WebhookHistory historizaRequest(String request) {
		WebhookHistory webhookHistory = WebhookHistory.getBuilder().request(request).build();
		repositorioWebhook.save(webhookHistory);
		return webhookHistory;
	}

	private WebhookResponseDto processaRequest(WebhookRequestDto requestDto) {
		RequestExecutor executor = ResquestExecutorFactory.getInstance(requestDto);
		return executor.execute();
	}

	private WebhookRequestDto parseRequest(String request) {
		return new Gson().fromJson(request, WebhookRequestDto.class);
	}
}
