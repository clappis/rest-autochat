package autochat.services.prestador;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class PrestadorDTO {

	@NotBlank(message = "É necessário informar o nome do prestador.")
	private String nome;
	
	@NotBlank(message = "É necessário informar o documento do prestador.")
	private String documento;
	
	@NotNull(message = "É necessário informar o facebookId do prestador.")
	private Long facebookId;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public Long getFacebookId() {
		return facebookId;
	}
	public void setFacebookId(Long facebookId) {
		this.facebookId = facebookId;
	}
	
	
}
