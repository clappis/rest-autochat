package autochat.services.prestador;

import static autochat.models.prestador.Prestador.getBuilder;

import java.util.ArrayList;
import java.util.List;

import autochat.models.prestador.Prestador;

class PrestadorAdapter {

	PrestadorDTO adapt(Prestador prestador){
		PrestadorDTO dto = new PrestadorDTO();
		dto.setDocumento(prestador.getDocumento());
		dto.setFacebookId(prestador.getFacebookId());
		dto.setNome(prestador.getNome());
		return dto;
	}

	List<PrestadorDTO> adapt(List<Prestador> prestadores){
		List<PrestadorDTO> dtos = new ArrayList<>();

		for (Prestador prestador : prestadores) 
			dtos.add(adapt(prestador));
		
		return dtos;
	}

	public Prestador adapt(PrestadorDTO dto) {
		return getBuilder()
			.nome(dto.getNome())
			.documento(dto.getDocumento())
			.facebookId(dto.getFacebookId())
			.build();
			
	}
}
