package autochat.services.prestador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import autochat.models.prestador.Prestador;
import autochat.models.prestador.PrestadorRepositorio;

@Service
public class PrestadorService {

	@Autowired
	private PrestadorRepositorio repositorio;
	private PrestadorAdapter adapter = new PrestadorAdapter();
	
	public PrestadorAdapter getAdapter() {
		return adapter;
	}

	public PrestadorRepositorio getRepositorio() {
		return repositorio;
	}
	
	public List<PrestadorDTO> consulta() {
		return getAdapter().adapt(repositorio.findAll());
	}

	public void inserir(PrestadorDTO dto) {
		Prestador prestador = getAdapter().adapt(dto);
		getRepositorio().save(prestador);
	}

	public void deletar(Long facebookId) {
		Prestador prestador = getRepositorio().findByFacebookId(facebookId);
		if (prestador != null)
			getRepositorio().delete(prestador);
	}

	public PrestadorDTO consulta(Long facebookId) {
		Prestador prestador = getRepositorio().findByFacebookId(facebookId);
		if (prestador == null)
			return null;

		return getAdapter().adapt(prestador);
	}
}
