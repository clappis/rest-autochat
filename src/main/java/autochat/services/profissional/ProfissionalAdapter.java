package autochat.services.profissional;

import static autochat.models.profissional.Profissional.getBuilder;

import java.util.ArrayList;
import java.util.List;

import autochat.models.profissional.Profissional;

public class ProfissionalAdapter {

	public Profissional adapt(ProfissionalDTO dto) {
		return getBuilder()
			.nome(dto.getNome())
			.cpf(dto.getCpf())
			.build();
	}

	public List<ProfissionalDTO> adapt(List<Profissional> profissionais) {
		List<ProfissionalDTO> dtos = new ArrayList<>();
		
		for (Profissional profissional : profissionais) 
			dtos.add(adapt(profissional));
		
		return dtos;
	}

	public ProfissionalDTO adapt(Profissional profissional) {
		ProfissionalDTO dto = new ProfissionalDTO();
		dto.setId(profissional.getId());
		dto.setNome(profissional.getNome());
		dto.setCpf(profissional.getCpf());
		return dto;
	}

}
