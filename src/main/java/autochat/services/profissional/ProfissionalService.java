package autochat.services.profissional;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import autochat.models.profissional.Profissional;
import autochat.models.profissional.ProfissionalRepositorio;
import autochat.models.servico.Servico;
import autochat.models.servico.ServicoRepositorio;

@Service
public class ProfissionalService {

	@Autowired ProfissionalRepositorio repositorioProfissional;
	@Autowired ServicoRepositorio repositorioServico;
	private ProfissionalAdapter adapter = new ProfissionalAdapter();
	
	protected ProfissionalRepositorio getRepositorioProfissional() {
		return repositorioProfissional;
	}

	protected ServicoRepositorio getRepositorioServico() {
		return repositorioServico;
	}
	
	private ProfissionalAdapter getAdapter() {
		return adapter;
	}

	public List<ProfissionalDTO> consulta(){
		return getAdapter().adapt(getRepositorioProfissional().findAll());
	}

	public List<ProfissionalDTO> consulta(long idServico) {
		Servico servico = getRepositorioServico().findById(idServico);
		if (servico == null)
			return new ArrayList<>();
		
		List<Profissional> profissionais = getRepositorioProfissional().consulta(servico);
		return getAdapter().adapt(profissionais);
	}

	public void inserir(ProfissionalDTO dto) {
		Profissional profissional = adapter.adapt(dto);
		getRepositorioProfissional().save(profissional);
	}

	public void adicionaServico(long id, long idServico) {
		Profissional profissional = getRepositorioProfissional().findById(id);
		Servico servico = getRepositorioServico().findById(idServico);
		if (profissional == null || servico == null)
			return;
		
		profissional.addServicos(servico);
		repositorioProfissional.save(profissional);
	}
	
}
