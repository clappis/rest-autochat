package autochat.services.atendimento;

import static autochat.models.atendimento.IntervaloAtendimento.getBuilder;

import autochat.models.atendimento.AtendimentoPadrao;
import autochat.models.atendimento.IntervaloAtendimento;

public class IntervaloAtendimentoAdapter {

	public IntervaloAtendimentoDTO adapt(IntervaloAtendimento intervalo) {
		IntervaloAtendimentoDTO dto = new IntervaloAtendimentoDTO();
		dto.setDataInicio(intervalo.getDataInicio());
		dto.setDataFim(intervalo.getDataFim());
		return dto;
	}

	public IntervaloAtendimento adapt(IntervaloAtendimentoDTO dto, AtendimentoPadrao atendimento) {
		return getBuilder()
				.dataInicio(dto.getDataInicio())
				.dataFim(dto.getDataFim())
				.atendimento(atendimento)
				.build();
	}

}
