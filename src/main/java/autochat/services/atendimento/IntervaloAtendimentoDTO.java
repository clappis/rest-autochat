package autochat.services.atendimento;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class IntervaloAtendimentoDTO {

	@JsonFormat(pattern = "HH:mm", timezone = "Brazil/East")
	private Date dataInicio;
	
	@JsonFormat(pattern = "HH:mm", timezone = "Brazil/East")
	private Date dataFim;

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
}
