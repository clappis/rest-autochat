package autochat.services.atendimento;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import autochat.models.atendimento.AtendimentoPadrao;
import autochat.models.atendimento.AtendimentoPadraoRepositorio;
import autochat.models.atendimento.IntervaloAtendimento;
import autochat.models.atendimento.IntervaloAtendimentoRepositorio;
import autochat.models.profissional.Profissional;
import autochat.models.profissional.ProfissionalRepositorio;

@Service
public class AtendimentoService {

	@Autowired private AtendimentoPadraoRepositorio repositorioAtendimento;
	@Autowired private ProfissionalRepositorio repositorioProfissional;
	@Autowired private IntervaloAtendimentoRepositorio repositorioIntervalo;
	private AtendimentoAdapter atendimentoAdapter = new AtendimentoAdapter();
	private IntervaloAtendimentoAdapter intervaloAdapter = new IntervaloAtendimentoAdapter();
	
	protected IntervaloAtendimentoAdapter getIntervaloAdapter() {
		return intervaloAdapter;
	}

	private AtendimentoAdapter getAtendimentoAdapter() {
		return atendimentoAdapter;
	}
	
	private IntervaloAtendimentoRepositorio getRepositorioIntervalo() {
		return repositorioIntervalo;
	}

	private ProfissionalRepositorio getRepositorioProfissional() {
		return repositorioProfissional;
	}

	private AtendimentoPadraoRepositorio getRepositorioAtendimento() {
		return repositorioAtendimento;
	}

	public AtendimentoDTO consulta(Long idProfissional, Integer dia) {
		Profissional profissional = getRepositorioProfissional().findById(idProfissional);
		if (profissional == null)
			return null;
		
		AtendimentoPadrao atendimento = getRepositorioAtendimento().consulta(profissional, dia);
		AtendimentoDTO dto = getAtendimentoAdapter().adapt(atendimento);
		return dto;
	}

	public void inserir(AtendimentoDTO dto) {
		Profissional profissional = getRepositorioProfissional().findById(dto.getIdProfissional());
		AtendimentoPadrao atendimento = getAtendimentoAdapter().adapt(dto, profissional);
		getRepositorioAtendimento().save(atendimento);
		
		for (IntervaloAtendimentoDTO intervaloDTO : dto.getIntervalos()) {
			IntervaloAtendimento intervalo = getIntervaloAdapter().adapt(intervaloDTO, atendimento);
			getRepositorioIntervalo().save(intervalo);
		}
	}

	public void deletar(long id) {
		throw new RuntimeException();
	}

}
