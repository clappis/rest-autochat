package autochat.services.atendimento;

import java.util.HashSet;
import java.util.Set;

public class AtendimentoDTO {

	private Long idProfissional;
	
	private Integer dia;
	
	private Long id;
	
	private Set<IntervaloAtendimentoDTO> intervalos = new HashSet<IntervaloAtendimentoDTO>();

	public Set<IntervaloAtendimentoDTO> getIntervalos() {
		return intervalos;
	}

	public void addIntervalo(IntervaloAtendimentoDTO intervalo){
		intervalos.add(intervalo);
	}
	
	public Long getIdProfissional() {
		return idProfissional;
	}

	public void setIdProfissional(Long idProfissional) {
		this.idProfissional = idProfissional;
	}

	public Integer getDia() {
		return dia;
	}

	public void setDia(Integer dia) {
		this.dia = dia;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	} 
}
