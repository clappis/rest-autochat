package autochat.services.atendimento;

import autochat.models.atendimento.AtendimentoPadrao;
import autochat.models.atendimento.IntervaloAtendimento;
import autochat.models.profissional.Profissional;

public class AtendimentoAdapter {
	
	private IntervaloAtendimentoAdapter intervaloAdapter = new IntervaloAtendimentoAdapter();

	protected IntervaloAtendimentoAdapter getIntervaloAdapter() {
		return intervaloAdapter;
	}

	public AtendimentoDTO adapt(AtendimentoPadrao atendimento) {
		if (atendimento == null)
			return null;
		
		AtendimentoDTO dto = new AtendimentoDTO();
		
		for (IntervaloAtendimento intervalo : atendimento.getIntervalos()) 
			dto.addIntervalo(getIntervaloAdapter().adapt(intervalo));
		
		dto.setIdProfissional(atendimento.getProfissional().getId());
		dto.setDia(atendimento.getDia());
		dto.setId(atendimento.getId());
		return dto;
	}

	public AtendimentoPadrao adapt(AtendimentoDTO dto, Profissional profissional) {
		if (profissional == null)
			throw new RuntimeException("blabla arrumar");
		
		AtendimentoPadrao atendimento = AtendimentoPadrao.getBuilder()
			.profissional(profissional)
			.dia(dto.getDia())
			.build();
		
		return atendimento;
	}

}
