package autochat.services.agendamento;

import java.util.Date;

import javax.validation.constraints.NotNull;

public class RequisicaoAgendamentoDto {

	@NotNull(message = "É necessário informar o serviço.")
	public Long idServico;
	@NotNull(message = "É necessário informar o profissional.")
	public Long idProfissional;
	@NotNull(message = "É necessário informar a data inicio.")
	public Date dataInicio;
	@NotNull(message = "É necessário informar a data fim.")
	public Date dataFim;
	
	public Long getIdServico() {
		return idServico;
	}
	public void setIdServico(Long idServico) {
		this.idServico = idServico;
	}
	public Long getIdProfissional() {
		return idProfissional;
	}
	public void setIdProfissional(Long idProfissional) {
		this.idProfissional = idProfissional;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
}
