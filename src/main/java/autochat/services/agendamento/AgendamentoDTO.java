package autochat.services.agendamento;

import java.util.Date;

import autochat.services.profissional.ProfissionalDTO;
import autochat.services.servico.ServicoDTO;

public class AgendamentoDTO {

	private ServicoDTO servico;
	
	private ProfissionalDTO profissional;
	
	private Date dataInicio;
	
	private Date dataFim;

	public ServicoDTO getServico() {
		return servico;
	}

	public void setServico(ServicoDTO servico) {
		this.servico = servico;
	}

	public ProfissionalDTO getProfissional() {
		return profissional;
	}

	public void setProfissional(ProfissionalDTO profissional) {
		this.profissional = profissional;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
}
