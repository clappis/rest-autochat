package autochat.services.agendamento;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ConsultaAgendamentoDto {

//	@NotNull(message = "É necessário informar o serviço.")
	public Long idServico;
//	@NotNull(message = "É necessário informar o profissional.")
	public Long idProfissional;
//	@NotNull(message = "É necessário informar o dia.")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "Brazil/East")
	public Date dia;
	
	public Long getIdServico() {
		return idServico;
	}
	public void setIdServico(Long idServico) {
		this.idServico = idServico;
	}
	public Long getIdProfissional() {
		return idProfissional;
	}
	public void setIdProfissional(Long idProfissional) {
		this.idProfissional = idProfissional;
	}
	public Date getDia() {
		return dia;
	}
	public void setDia(Date dia) {
		this.dia = dia;
	}
	
}
