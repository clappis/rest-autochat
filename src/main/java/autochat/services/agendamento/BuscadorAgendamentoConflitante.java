package autochat.services.agendamento;

import java.util.Date;
import java.util.List;

import autochat.models.agendamento.Agendamento;

public class BuscadorAgendamentoConflitante {

	public Agendamento busca(List<Agendamento> agendamentos, Date dataInicio, Date dataFim){
		for (Agendamento agendamento : agendamentos) {
			if (dataInicio.after(agendamento.getDataInicio()) && dataInicio.before(agendamento.getDataFim()))
				return agendamento;
			if (dataFim.after(agendamento.getDataInicio()) && dataFim.before(agendamento.getDataFim()))
				return agendamento;
			if (dataInicio.equals(agendamento.getDataInicio()) && dataFim.equals(agendamento.getDataFim()))
				return agendamento;
		}
		
		return null;
	}
}
