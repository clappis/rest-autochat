package autochat.services.agendamento;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import autochat.models.agendamento.AgendamentoRepositorio;
import autochat.models.atendimento.AtendimentoPadraoRepositorio;
import autochat.models.profissional.ProfissionalRepositorio;
import autochat.models.servico.ServicoRepositorio;
import autochat.services.profissional.ProfissionalAdapter;
import autochat.services.servico.ServicoAdapter;

@Service
public class AgendamentoService {

	@Autowired
	AgendamentoRepositorio repositorioAgendamento;

	@Autowired
	ProfissionalRepositorio repositorioProfissional;

	@Autowired
	ServicoRepositorio repositorioServico;

	@Autowired
	AtendimentoPadraoRepositorio repositorioAtendimento;

	@Autowired
	GeradorAgendamentoTransiente geradorAgendamentoTransiente;
	
	ProfissionalAdapter profissionalAdapter = new ProfissionalAdapter();
	ServicoAdapter servicoAdapter = new ServicoAdapter();
	
	protected GeradorAgendamentoTransiente getGeradorAgendamentoTransiente() {
		return geradorAgendamentoTransiente;
	}

	protected ServicoAdapter getServicoAdapter() {
		return servicoAdapter;
	}

	protected ProfissionalAdapter getProfissionalAdapter() {
		return profissionalAdapter;
	}

	protected AgendamentoRepositorio getRepositorioAgendamento() {
		return repositorioAgendamento;
	}

	protected ProfissionalRepositorio getRepositorioProfissional() {
		return repositorioProfissional;
	}

	protected ServicoRepositorio getRepositorioServico() {
		return repositorioServico;
	}

	protected AtendimentoPadraoRepositorio getRepositorioAtendimento() {
		return repositorioAtendimento;
	}

	public List<AgendamentoDTO> consulta(ConsultaAgendamentoDto dto) {
		return getGeradorAgendamentoTransiente().gerar(dto);
	}

	public void agendar(RequisicaoAgendamentoDto dto) {

	}

}
