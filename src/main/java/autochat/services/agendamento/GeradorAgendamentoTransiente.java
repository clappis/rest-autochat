package autochat.services.agendamento;

import static java.util.Calendar.DAY_OF_WEEK;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import autochat.models.agendamento.Agendamento;
import autochat.models.agendamento.AgendamentoRepositorio;
import autochat.models.atendimento.AtendimentoPadrao;
import autochat.models.atendimento.AtendimentoPadraoRepositorio;
import autochat.models.atendimento.IntervaloAtendimento;
import autochat.models.profissional.Profissional;
import autochat.models.profissional.ProfissionalRepositorio;
import autochat.models.servico.Servico;
import autochat.models.servico.ServicoRepositorio;
import autochat.services.profissional.ProfissionalAdapter;
import autochat.services.servico.ServicoAdapter;

@Service
public class GeradorAgendamentoTransiente {

	@Autowired
	AgendamentoRepositorio repositorioAgendamento;
	
	@Autowired
	ProfissionalRepositorio repositorioProfissional;
	
	@Autowired
	ServicoRepositorio repositorioServico;
	
	@Autowired
	AtendimentoPadraoRepositorio repositorioAtendimento;
	
	ProfissionalAdapter profissionalAdapter = new ProfissionalAdapter();
	ServicoAdapter servicoAdapter = new ServicoAdapter();
	BuscadorAgendamentoConflitante buscadorAgendamentoConflifante = new BuscadorAgendamentoConflitante();
	
	protected BuscadorAgendamentoConflitante getBuscadorAgendamentoConflitante() {
		return buscadorAgendamentoConflifante;
	}

	protected ServicoAdapter getServicoAdapter() {
		return servicoAdapter;
	}

	protected ProfissionalAdapter getProfissionalAdapter() {
		return profissionalAdapter;
	}

	protected AgendamentoRepositorio getRepositorioAgendamento() {
		return repositorioAgendamento;
	}

	protected ProfissionalRepositorio getRepositorioProfissional() {
		return repositorioProfissional;
	}

	protected ServicoRepositorio getRepositorioServico() {
		return repositorioServico;
	}
	
	protected AtendimentoPadraoRepositorio getRepositorioAtendimento() {
		return repositorioAtendimento;
	}
	
	List<AgendamentoDTO> gerar(ConsultaAgendamentoDto dto){
		Profissional profissional = getRepositorioProfissional().findById(dto.getIdProfissional());
		Servico servico = getRepositorioServico().findById(dto.getIdServico());
		List<Agendamento> agendamentos = getRepositorioAgendamento().consulta(profissional, dto.getDia());
		AtendimentoPadrao atendimentoPadrao = getAtendimentoPadao(profissional, dto.getDia());
		List<AgendamentoDTO> agendamentosTransientes = new ArrayList<>();
		
		for (IntervaloAtendimento intervalo : atendimentoPadrao.getIntervalos()) {
			Date dataInicio = intervalo.getDataInicio();
			Date dataFimIntervalo = intervalo.getDataFim();
			
			while(dataInicio.before(dataFimIntervalo)) {
				Date dataFim = getDataFimServico(servico, dataInicio);
				Agendamento agendamento = getBuscadorAgendamentoConflitante().busca(agendamentos, dataInicio, dataFim);
				
				if (agendamento != null) {
					dataInicio = agendamento.getDataFim();
					continue;
				}
				
				if (dataFim.after(dataFimIntervalo))
					break;

				AgendamentoDTO agendamentoDTO = new AgendamentoDTO();
				agendamentoDTO.setProfissional(getProfissionalAdapter().adapt(profissional));
				agendamentoDTO.setServico(getServicoAdapter().adapt(servico));
				agendamentoDTO.setDataInicio(dataInicio);
				agendamentoDTO.setDataFim(dataFim);
				agendamentosTransientes.add(agendamentoDTO);
				dataInicio = dataFim;
			}
		}
		
		return agendamentosTransientes;
	}

	private Date getDataFimServico(Servico servico, Date dataInicio) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dataInicio);
		calendar.add(Calendar.MINUTE, servico.getTempoAtendimento());
		Date dataFim = calendar.getTime();
		return dataFim;
	};
	
	AtendimentoPadrao getAtendimentoPadao(Profissional profissional, Date data){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		return getRepositorioAtendimento().consulta(profissional, calendar.get(DAY_OF_WEEK));
	}
}
