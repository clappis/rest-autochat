package autochat.controllers.atendimento;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import autochat.services.atendimento.AtendimentoDTO;
import autochat.services.atendimento.AtendimentoService;

@RestController
@RequestMapping("/atendimento")
public class AtendimentoController {

	@Autowired
	AtendimentoService service;

	@RequestMapping("/consultaPorProfissional")
	public AtendimentoDTO consulta(@RequestParam("idProfissional") Long idProfissional, @RequestParam("dia") Integer dia){
		return service.consulta(idProfissional, dia);
	}
	
	@RequestMapping(value = "/inserir", method = RequestMethod.POST)
	public void inserir(@Valid @RequestBody AtendimentoDTO dto){
		service.inserir(dto);
	}
	
	@RequestMapping(value = "/deletar", method = RequestMethod.POST)
	public void deletar(@Valid @RequestBody long id){
		service.deletar(id);
	}
}
