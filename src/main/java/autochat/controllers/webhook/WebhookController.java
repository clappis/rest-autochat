package autochat.controllers.webhook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;

import autochat.services.webhook.WebhookService;
import autochat.services.webhook.request.WebhookRequestDto;
import autochat.services.webhook.response.WebhookResponseDto;

@RestController
@RequestMapping("/webhook")
public class WebhookController {

	private final static Logger logger = LoggerFactory.getLogger(WebhookController.class);
	 
	@Autowired
	WebhookService service;
	
	@RequestMapping(method = RequestMethod.POST)
	public WebhookResponseDto inserir(@RequestBody String request){
		if (true)
			return service.processaRequest(request);
		
		ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        WebhookResponseDto response = new WebhookResponseDto();
        
		logger.info(request);
		WebhookRequestDto requestDto = new Gson().fromJson(request, WebhookRequestDto.class);
		response.setSource("ID FACEBOOK:" + requestDto.getOriginalRequest().getData().getSender().getId());
		response.setDisplayText("ALOHA");
		return response;
	}
	
//	@RequestMapping(method = RequestMethod.POST)
//	public WebHookResponseDto inserir(@RequestBody WebHookRequestDto dto){
//		ObjectMapper mapper = new ObjectMapper();
//        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
//        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
//        WebHookResponseDto response = new WebHookResponseDto();
//        
//		try {
//			logger.info(mapper.writeValueAsString(dto));
//			response.setSpeech("opa");
//		} catch (JsonProcessingException e) {
//			response.setSpeech(e.getMessage());
//		}
//        
//		
//		response.setDisplayText("ALOHA");
////		if (!dto.getResult().getFulfillment().getMessages().isEmpty()){
////			response.setSpeech(dto.getResult().getFulfillment().getMessages().get(0).getSpeech());
////		}
////		response.setDisplayText("TESTE");
////		response.setData(new WebHookResponseData());
////		response.setContextOut(new ArrayList<WebHookContextDto>());
////		response.setSource("DuckXD");
//		return response;
//	}
	
//	@RequestMapping(method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE, 
//            produces=MediaType.TEXT_MARKDOWN_VALUE)
//	public ResponseEntity<String> addWebHook(@RequestBody WebHook webHook){
//		logger.info("New webhook for " + webHook.getCompanyName() + " is registered");
//		
//		if(webHookRepositorio.findById(webHook.getId()) != null)
//			return new ResponseEntity<>("Webhook already exists", HttpStatus.OK);
//		
//		webHookRepositorio.save(webHook);
//		return new ResponseEntity<>("Successfully", HttpStatus.CREATED);
//	}
//	
//	@RequestMapping(method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<List<WebHook>> getAllWebHooks(){
//		return new ResponseEntity<List<WebHook>>(webHookRepositorio.findAll(), HttpStatus.OK);
//	}
//	
//	@RequestMapping(method = RequestMethod.GET, 
//           value ="/comapnies/{companyName}/types/{type}", 
//produces=MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<WebHook> getWebHooksByCompanyNameAndType(
//                @PathVariable String companyName, 
//                @PathVariable String type){
//		return new ResponseEntity<WebHook>(webHookRepositorio.findById(1L), HttpStatus.OK);
//	}
//	
//	@RequestMapping(method = RequestMethod.DELETE,
//           value ="/comapnies/{companyName}/types/{type}", 
//produces=MediaType.TEXT_MARKDOWN_VALUE)
//	public ResponseEntity<String> removeWebHook(
//               @PathVariable String companyName, 
//               @PathVariable String type){
//		
//		WebHook webhook = webHookRepositorio.findById(1L);
//		if(webhook != null){
//			webHookRepositorio.delete(webhook);
//			return new ResponseEntity<>("WebHook was successfully deleted.", HttpStatus.OK);
//		}
//		return new ResponseEntity<>("Webhook doesn't exist.", HttpStatus.OK);
//	}
//	
//	@RequestMapping(method = RequestMethod.DELETE,value ="/ids/{id}", 
//produces=MediaType.TEXT_MARKDOWN_VALUE)
//	public ResponseEntity<String> removeWebHookById(@PathVariable Long id){
//		WebHook webhook= webHookRepositorio.findOne(id);
//		if(webhook != null){
//			webHookRepositorio.delete(webhook);
//			return new ResponseEntity<>("WebHook was successfully deleted.", HttpStatus.OK);
//		}
//		return new ResponseEntity<>("Webhook doesn't exist.", HttpStatus.OK);
//	}
}
