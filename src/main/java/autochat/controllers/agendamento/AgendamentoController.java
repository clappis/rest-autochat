package autochat.controllers.agendamento;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import autochat.services.agendamento.AgendamentoDTO;
import autochat.services.agendamento.AgendamentoService;
import autochat.services.agendamento.ConsultaAgendamentoDto;
import autochat.services.agendamento.RequisicaoAgendamentoDto;

@RestController
@RequestMapping("/agendamento")
public class AgendamentoController {

	@Autowired
	AgendamentoService service;
	
	@RequestMapping(value = "/consultaHorariosLivres", method = RequestMethod.POST)
	public List<AgendamentoDTO> consulta(@RequestBody ConsultaAgendamentoDto dto){
		return service.consulta(dto);
	}
	
	@RequestMapping(value = "/agendar", method = RequestMethod.POST)
	public void agendar(@RequestBody @Valid RequisicaoAgendamentoDto dto){
		service.agendar(dto);
	}
}
