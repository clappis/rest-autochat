package autochat.controllers.prestador;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import autochat.services.prestador.PrestadorDTO;
import autochat.services.prestador.PrestadorService;

@RestController
@RequestMapping("/prestador")
public class PrestadorController {

	@Autowired
	PrestadorService service;

	@RequestMapping("/consulta")
	public List<PrestadorDTO> consulta(){
		return service.consulta();
	}
	
	@RequestMapping("/consultaPorFacebookId")
	public PrestadorDTO consulta(Long facebookId){
		return service.consulta(facebookId);
	}
	
	@RequestMapping(value = "/inserir", method = RequestMethod.POST)
	public void inserir(@Valid @RequestBody PrestadorDTO dto){
		service.inserir(dto);
	}
	
	@RequestMapping(value = "/deletar", method = RequestMethod.POST)
	public void deletar(@Valid @RequestBody long id){
		service.deletar(id);
	}
}
