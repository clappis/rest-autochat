package autochat.controllers.profissional;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import autochat.services.profissional.ProfissionalDTO;
import autochat.services.profissional.ProfissionalService;

@RestController
@RequestMapping("/profissional")
public class ProfissionalController {

	@Autowired
	ProfissionalService service;
	
	@RequestMapping("/consultaPorServico")
	public List<ProfissionalDTO> consulta(@RequestParam(value="idServico", required=true) Long idServico){
		return service.consulta(idServico);
	}
	
	@RequestMapping("/consulta")
	public List<ProfissionalDTO> consulta(){
		return service.consulta();
	}
	
	@RequestMapping(value = "/inserir", method = RequestMethod.POST)
	public void inserir(@Valid @RequestBody ProfissionalDTO dto){
		service.inserir(dto);
	}
	
	@RequestMapping(value = "/adicionarServico", method = RequestMethod.POST)
	public void adicionaServico(long id, long idServico){
		service.adicionaServico(id, idServico);
	}
	
}
