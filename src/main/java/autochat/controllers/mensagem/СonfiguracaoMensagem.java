package autochat.controllers.mensagem;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import autochat.models.Entidade;
import autochat.models.prestador.Prestador;
import autochat.models.profissional.Profissional;

@Entity
public class СonfiguracaoMensagem extends Entidade{

	@Column
	private String mensagem;
	
	@OneToOne
	@JoinColumn(nullable=false)
	private Prestador prestador;
	
	@Column
	private String acao;
	
	public String getMensagem() {
		return mensagem;
	}
	protected void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public Prestador getPrestador() {
		return prestador;
	}
	protected void setPrestador(Prestador prestador) {
		this.prestador = prestador;
	}
	public String getAcao() {
		return acao;
	}
	protected void setAcao(String acao) {
		this.acao = acao;
	}
}
