package autochat.controllers.mensagem;

import org.springframework.data.jpa.repository.Query;

import autochat.models.EntidadeRepositorio;
import autochat.models.prestador.Prestador;

public interface ConfiguracaoMensagemRepositorio extends EntidadeRepositorio<СonfiguracaoMensagem> {

	@Query("select a from Configuracao_Atendimento c where c.prestador = :prestador and upper(c.acao) = (:acao)")
	СonfiguracaoMensagem findByPrestador(Prestador prestador, String acao);
}
