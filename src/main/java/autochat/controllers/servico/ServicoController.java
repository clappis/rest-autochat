package autochat.controllers.servico;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import autochat.services.servico.ServicoDTO;
import autochat.services.servico.ServicoService;

@RestController
@RequestMapping("/servico")
public class ServicoController {

	@Autowired
	ServicoService service;

	@RequestMapping("/consulta")
	public List<ServicoDTO> consulta(){
		return service.consulta();
	}
	
	@RequestMapping(value = "/inserir", method = RequestMethod.POST)
	public void inserir(@Valid @RequestBody ServicoDTO dto){
		service.inserir(dto);
	}
	
	@RequestMapping(value = "/deletar", method = RequestMethod.POST)
	public void deletar(@Valid @RequestBody Long id){
		service.deletar(id);
	}
}
