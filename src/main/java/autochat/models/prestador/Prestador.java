package autochat.models.prestador;

import javax.persistence.Column;
import javax.persistence.Entity;

import autochat.models.Entidade;

@Entity
public class Prestador extends Entidade {

	@Column(nullable=false)
	private String nome;
	
	@Column(nullable=false)
	private String documento;
	
	@Column(nullable=false)
	private Long facebookId;
	
	public String getNome() {
		return nome;
	}

	void setNome(String nome) {
		this.nome = nome;
	}

	public String getDocumento() {
		return documento;
	}

	void setDocumento(String documento) {
		this.documento = documento;
	}

	public Long getFacebookId() {
		return facebookId;
	}

	void setFacebookId(Long facebookId) {
		this.facebookId = facebookId;
	}

	public static PrestadorBuilder getBuilder(){
		return new PrestadorBuilder();
	}
}
