package autochat.models.prestador;

public class PrestadorBuilder {

	private Prestador prestador;

	PrestadorBuilder(){
		prestador = new Prestador();
	}

	public PrestadorBuilder nome(String nome) {
		prestador.setNome(nome);
		return this;
	}

	public PrestadorBuilder documento(String documento) {
		prestador.setDocumento(documento);
		return this;
	}

	public PrestadorBuilder facebookId(Long facebookId) {
		prestador.setFacebookId(facebookId);
		return this;
	}

	public Prestador build() {
		return prestador;
	}
}
