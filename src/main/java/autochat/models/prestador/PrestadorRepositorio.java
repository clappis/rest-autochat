package autochat.models.prestador;

import org.springframework.data.repository.query.Param;

import autochat.models.EntidadeRepositorio;

public interface PrestadorRepositorio extends EntidadeRepositorio<Prestador> {
	
	Prestador findByFacebookId(@Param("facebookId") Long facebookId);
	
}
