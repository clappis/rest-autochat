package autochat.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface EntidadeRepositorio<T> extends JpaRepository<T, Long> {

	@Query
	public T findById(Long id);
}
