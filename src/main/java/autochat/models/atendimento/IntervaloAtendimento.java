package autochat.models.atendimento;

import static javax.persistence.TemporalType.TIME;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

import autochat.models.Entidade;

@Entity
public class IntervaloAtendimento extends Entidade {

	@Temporal(TIME)
	@Column(nullable=false)
	private Date dataInicio;
	
	@Temporal(TIME)
	@Column(nullable=false)
	private Date dataFim;
	
	@ManyToOne
	@JoinColumn(name = "agendamento_id")
	private AtendimentoPadrao atendimento;

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public AtendimentoPadrao getAtendimento() {
		return atendimento;
	}

	public void setAtendimento(AtendimentoPadrao atendimento) {
		this.atendimento = atendimento;
	}

	public static IntervaloAtendimentoBuilder getBuilder() {
		return new IntervaloAtendimentoBuilder();
	}
}
