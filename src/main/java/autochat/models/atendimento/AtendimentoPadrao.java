package autochat.models.atendimento;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.validator.constraints.Range;

import autochat.models.Entidade;
import autochat.models.profissional.Profissional;

@Entity
public class AtendimentoPadrao extends Entidade {

	AtendimentoPadrao(){}
	
	@OneToOne
	@JoinColumn(nullable=false)
	private Profissional profissional;
	
	@Range(min = 0, max = 6)
	@Column(nullable=false)
	private Integer dia;
	
	@OneToMany(mappedBy = "atendimento")
	private Set<IntervaloAtendimento> intervalos = new HashSet<IntervaloAtendimento>();

	public Profissional getProfissional() {
		return profissional;
	}

	void setProfissional(Profissional profissional) {
		this.profissional = profissional;
	}

	public Integer getDia() {
		return dia;
	}

	void setDia(Integer dia) {
		this.dia = dia;
	}

	public Set<IntervaloAtendimento> getIntervalos() {
		return intervalos;
	}

	public static AtendimentoPadraoBuilder getBuilder() {
		return new AtendimentoPadraoBuilder();
	} 
}
