package autochat.models.atendimento;

import autochat.models.EntidadeRepositorio;

public interface IntervaloAtendimentoRepositorio extends EntidadeRepositorio<IntervaloAtendimento> {

}
