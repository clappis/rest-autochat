package autochat.models.atendimento;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import autochat.models.EntidadeRepositorio;
import autochat.models.profissional.Profissional;

public interface AtendimentoPadraoRepositorio extends EntidadeRepositorio<AtendimentoPadrao> {

	@Query("select a from AtendimentoPadrao a where a.profissional = :profissional and a.dia = :dia ")
	AtendimentoPadrao consulta(@Param("profissional") Profissional profissional,@Param("dia") Integer dia);

}
