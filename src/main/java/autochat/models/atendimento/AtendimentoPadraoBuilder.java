package autochat.models.atendimento;

import autochat.models.profissional.Profissional;

public class AtendimentoPadraoBuilder {

	private AtendimentoPadrao atendimento;

	AtendimentoPadraoBuilder(){
		atendimento = new AtendimentoPadrao();
	}
	
	public AtendimentoPadraoBuilder profissional(Profissional profissional) {
		atendimento.setProfissional(profissional);
		return this;
	}

	public AtendimentoPadraoBuilder dia(Integer dia) {
		atendimento.setDia(dia);
		return this;
	}
	
	public AtendimentoPadrao build(){
		return atendimento;
	}

}
