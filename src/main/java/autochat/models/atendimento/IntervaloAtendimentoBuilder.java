package autochat.models.atendimento;

import java.util.Date;

public class IntervaloAtendimentoBuilder {

	private IntervaloAtendimento intervalo;

	IntervaloAtendimentoBuilder(){
		intervalo = new IntervaloAtendimento();
	}
	
	public IntervaloAtendimento build(){
		return intervalo;
	}

	public IntervaloAtendimentoBuilder dataInicio(Date dataInicio) {
		intervalo.setDataInicio(dataInicio);
		return this;
	}

	public IntervaloAtendimentoBuilder dataFim(Date dataFim) {
		intervalo.setDataFim(dataFim);
		return this;
	}

	public IntervaloAtendimentoBuilder atendimento(AtendimentoPadrao atendimento) {
		intervalo.setAtendimento(atendimento);
		return this;
	}
}
