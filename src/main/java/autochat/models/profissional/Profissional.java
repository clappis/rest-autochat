package autochat.models.profissional;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import autochat.models.Entidade;
import autochat.models.servico.Servico;

@Entity
public class Profissional extends Entidade {

	Profissional(){}
	
	@Column(nullable=false)
	private String nome;
	
	@Column(nullable=false)
	private String cpf;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="profissional_servico",  
			joinColumns=  @JoinColumn( name = "profissional_id"), 
			inverseJoinColumns= @JoinColumn(name = "servico_id")
			)
	private Set<Servico> servicos = new HashSet<Servico>(); 
	
	public Set<Servico> getServicos() {
		return servicos;
	}
	
	public void addServicos(Servico servico) {
		servicos.add(servico);
	}
	
	public String getNome() {
		return nome;
	}
	void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public static ProfissionalBuilder getBuilder() {
		return new ProfissionalBuilder();
	}
}
