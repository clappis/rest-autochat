package autochat.models.profissional;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import autochat.models.EntidadeRepositorio;
import autochat.models.servico.Servico;

public interface ProfissionalRepositorio extends EntidadeRepositorio<Profissional> {

	@Query("SELECT p from Profissional p where :servico MEMBER OF p.servicos")
	List<Profissional> consulta(@Param("servico") Servico servico);
}
