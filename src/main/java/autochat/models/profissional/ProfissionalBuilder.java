package autochat.models.profissional;

public class ProfissionalBuilder {

	private Profissional profissional;

	ProfissionalBuilder(){
		profissional = new Profissional();
	}

	public ProfissionalBuilder nome(String nome) {
		profissional.setNome(nome);
		return this;
	}

	public ProfissionalBuilder cpf(String cpf) {
		profissional.setCpf(cpf);
		return this;
	}

	public Profissional build() {
		return profissional;
	}
}
