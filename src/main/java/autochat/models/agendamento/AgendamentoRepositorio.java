package autochat.models.agendamento;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import autochat.models.EntidadeRepositorio;
import autochat.models.profissional.Profissional;

public interface AgendamentoRepositorio extends EntidadeRepositorio<Agendamento> {

	@Query("select a from Agendamento a where a.profissional = :profissional and " + 
			" (trunc(a.dataInicio) = trunc(:dia) or" + 
			" trunc(a.dataFim) = trunc(:dia))")
	List<Agendamento> consulta(@Param("profissional") Profissional profissional,
			@Param("dia") Date dia);
}
