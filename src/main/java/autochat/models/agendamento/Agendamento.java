package autochat.models.agendamento;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

import autochat.models.Entidade;
import autochat.models.profissional.Profissional;
import autochat.models.servico.Servico;

@Entity
public class Agendamento extends Entidade {

	@OneToOne
	@JoinColumn(nullable=false)
	private Servico servico;
	
	@OneToOne
	@JoinColumn(nullable=false)
	private Profissional profissional;
	
	@Temporal(TIMESTAMP)
	@Column(name="DATA_INICIO", nullable=false)
	private Date dataInicio;
	
	@Temporal(TIMESTAMP)
	@Column(name="DATA_FIM", nullable=false)
	private Date dataFim;

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public Profissional getProfissional() {
		return profissional;
	}

	public void setProfissional(Profissional profissional) {
		this.profissional = profissional;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
}
