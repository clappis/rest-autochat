package autochat.models.servico;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import autochat.models.Entidade;
import autochat.models.prestador.Prestador;

@Entity
public class Servico extends Entidade {

	@Column(nullable=false)
	private String nome;
	
	@Column(nullable=false)
	private int tempoAtendimento;
	
	@ManyToOne(optional=false)
	private Prestador prestador;
	
	public String getNome() {
		return nome;
	}
	void setNome(String nome) {
		this.nome = nome;
	}
	public int getTempoAtendimento() {
		return tempoAtendimento;
	}
	void setTempoAtendimento(int tempoAtendimento) {
		this.tempoAtendimento = tempoAtendimento;
	}
	
	public Prestador getPrestador() {
		return prestador;
	}
	void setPrestador(Prestador prestador) {
		this.prestador = prestador;
	}
	public static ServicoBuilder getBuilder(){
		return new ServicoBuilder();
	}
}
