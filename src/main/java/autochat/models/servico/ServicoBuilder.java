package autochat.models.servico;

import autochat.models.prestador.Prestador;

public class ServicoBuilder {

	private final Servico servico;
	
	ServicoBuilder(){
		servico = new Servico();
	}
	
	public ServicoBuilder nome(String nome){
		servico.setNome(nome);
		return this;
	}
	
	public ServicoBuilder tempoAtendimento(int tempoAtendimento){
		servico.setTempoAtendimento(tempoAtendimento);
		return this;
	}
	
	public ServicoBuilder prestador(Prestador prestador) {
		servico.setPrestador(prestador);
		return this;
	}
	
	public Servico build(){
		return servico;
	}

	
}
