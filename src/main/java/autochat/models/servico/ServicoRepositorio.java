package autochat.models.servico;

import autochat.models.EntidadeRepositorio;

public interface ServicoRepositorio extends EntidadeRepositorio<Servico> {}
