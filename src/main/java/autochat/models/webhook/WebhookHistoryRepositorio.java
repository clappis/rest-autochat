package autochat.models.webhook;

import autochat.models.EntidadeRepositorio;

public interface WebhookHistoryRepositorio extends EntidadeRepositorio<WebhookHistory> {

}
