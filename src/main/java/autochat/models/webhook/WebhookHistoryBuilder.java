package autochat.models.webhook;

import java.sql.Timestamp;

public class WebhookHistoryBuilder {

	private WebhookHistory webHook;

	public WebhookHistoryBuilder() {
		webHook = new WebhookHistory();
	}
	
	public WebhookHistoryBuilder request(String request){
		webHook.setRequest(request);
		return this;
	}
	
	public WebhookHistoryBuilder response(String response){
		webHook.setResponse(response);
		return this;
	}
	
	public WebhookHistory build(){
		webHook.setData(new Timestamp(System.currentTimeMillis()));
		return webHook;
	}
}
