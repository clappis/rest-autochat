package autochat.models.webhook;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Temporal;

import autochat.models.Entidade;

@Entity
public class WebhookHistory extends Entidade {
 
	@Lob
	@Column
	private String request;
	
	@Lob
	@Column
	private String response;

	@Temporal(TIMESTAMP)
	@Column(name="DATA_INICIO", nullable=false)
	private Date data;
	
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	Date getData() {
		return data;
	}
	void setData(Date data) {
		this.data = data;
	}

	public static WebhookHistoryBuilder getBuilder(){
		return new WebhookHistoryBuilder();
	}
}