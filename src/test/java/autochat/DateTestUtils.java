package autochat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTestUtils {

	public static Date parseData(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		try {
			return sdf.parse(date);
		} catch (ParseException e) {
			throw new RuntimeException();
		}
	}
}
