package autochat.services.profissional;

import static com.google.common.collect.Iterables.getOnlyElement;
import static org.assertj.core.util.Lists.newArrayList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import autochat.models.profissional.Profissional;
import autochat.models.profissional.ProfissionalRepositorio;
import autochat.models.servico.Servico;
import autochat.models.servico.ServicoRepositorio;

@RunWith(MockitoJUnitRunner.class)
public class ProfissionalServiceTest {

	private static final long ID = 1L;
	private static final String NOME_PROFISSIONAL = "Alan Clappis";
	private static final String CPF = "35088634846";

	@Spy ProfissionalService service;
	
	@Mock ServicoRepositorio repositorioServico;
	@Mock ProfissionalRepositorio repositorioProfissional;
	
	@Mock Servico servico;
	@Mock Profissional profissional;
	
	@Before
	public void setUp(){
		mockService();
		mockRepositorioServico();
		mockRepositorioProfissional();
		mockProfissional();
	}

	@Test
	public void consultaRepositorio(){
		assertThat(service.consulta(), hasSize(1));
		ProfissionalDTO dto = getOnlyElement(service.consulta());
		assertDto(dto);
	}

	@Test
	public void consultaRepositorioPorCodigo(){
		assertThat(service.consulta(1L), hasSize(1));
		ProfissionalDTO dto = getOnlyElement(service.consulta(1L));
		assertDto(dto);
	}
	
	private void assertDto(ProfissionalDTO dto) {
		assertThat(dto.getNome(), is(NOME_PROFISSIONAL));
		assertThat(dto.getCpf(), is(CPF));
		assertThat(dto.getId(), is(ID));
	}
	
	private void mockRepositorioServico() {
		when(repositorioServico.findById(anyLong())).thenReturn(servico);
	}

	private void mockRepositorioProfissional() {
		when(repositorioProfissional.findAll()).thenReturn(newArrayList(profissional));
		when(repositorioProfissional.consulta(servico)).thenReturn(newArrayList(profissional));
		when(repositorioProfissional.findById(anyLong())).thenReturn(profissional);
	}

	private void mockService() {
		when(service.getRepositorioProfissional()).thenReturn(repositorioProfissional);
		when(service.getRepositorioServico()).thenReturn(repositorioServico);
	}

	private void mockProfissional() {
		when(profissional.getCpf()).thenReturn(CPF);
		when(profissional.getNome()).thenReturn(NOME_PROFISSIONAL);
		when(profissional.getId()).thenReturn(ID);
	}

}
