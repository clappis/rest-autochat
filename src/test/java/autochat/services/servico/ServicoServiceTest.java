package autochat.services.servico;

import static com.google.common.collect.Iterables.getOnlyElement;
import static org.assertj.core.util.Lists.newArrayList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import autochat.models.prestador.Prestador;
import autochat.models.servico.Servico;
import autochat.models.servico.ServicoRepositorio;

@RunWith(MockitoJUnitRunner.class)
public class ServicoServiceTest {

	private static final int TEMPO_ATENDIMENTO = 15;
	private static final String NOME_SERVICO = "NOME";

	private static final long ID = 1L;

	@Spy
	ServicoService service;
	
	@Mock
	ServicoRepositorio repositorio;
	
	@Mock
	Servico servico;
	@Mock 
	Prestador prestador;
	
	@Before
	public void setUp(){
		mockService();
		mockRepositorio();
		mockServico();
	}

	@Test
	public void consultaRepositorio(){
		assertThat(service.consulta(), hasSize(1));
		ServicoDTO dto = getOnlyElement(service.consulta());
		assertDto(dto);
	}

	@Test
	public void consultaRepositorioPorCodigo(){
		assertDto(service.consulta(1L));
	}
	
	private void assertDto(ServicoDTO dto) {
		assertThat(dto.getId(), is(ID));
		assertThat(dto.getNome(), is(NOME_SERVICO));
		assertThat(dto.getTempoAtendimento(), is(TEMPO_ATENDIMENTO));
	}
	
	private void mockServico() {
		when(servico.getId()).thenReturn(ID);
		when(servico.getPrestador()).thenReturn(prestador);
		when(servico.getNome()).thenReturn(NOME_SERVICO);
		when(servico.getTempoAtendimento()).thenReturn(TEMPO_ATENDIMENTO);
	}

	private void mockRepositorio() {
		when(repositorio.findAll()).thenReturn(newArrayList(servico));
		when(repositorio.findById(anyLong())).thenReturn(servico);
	}

	private void mockService() {
		when(service.getRepositorioServico()).thenReturn(repositorio);
	}
}
