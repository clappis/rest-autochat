package autochat.services.agendamento;

import static autochat.DateTestUtils.parseData;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import autochat.models.agendamento.Agendamento;
import autochat.models.agendamento.AgendamentoRepositorio;
import autochat.models.atendimento.AtendimentoPadrao;
import autochat.models.atendimento.AtendimentoPadraoRepositorio;
import autochat.models.atendimento.IntervaloAtendimento;
import autochat.models.prestador.Prestador;
import autochat.models.profissional.Profissional;
import autochat.models.profissional.ProfissionalRepositorio;
import autochat.models.servico.Servico;
import autochat.models.servico.ServicoRepositorio;

@RunWith(MockitoJUnitRunner.class)
public class GeradorAgendamentoTransienteTest {

	private static final int TEMPO_ATENDIMENTO = 20;
	
	@Spy GeradorAgendamentoTransiente subject;
	@Mock AgendamentoRepositorio repositorioAgendamento;
	@Mock AtendimentoPadraoRepositorio repositorioAtendimento;
	@Mock ProfissionalRepositorio repositorioProfissional;
	@Mock ServicoRepositorio repositorioServico;
	@Mock Profissional profissional;
	@Mock Servico servico;
	@Mock Agendamento agendamento;
	@Mock AtendimentoPadrao atendimento;
	@Mock IntervaloAtendimento intervalo;
	@Mock Prestador prestador;
	@Mock ConsultaAgendamentoDto dto;

	
	@Before
	public void setUp(){
		mockSubject();
		mockServico();
		mockProfissional();
		mockAgendamento();
		mockAtendimento();
		mockDto();
	}

	private void mockDto() {
		when(dto.getDia()).thenReturn(parseData("31-08-1982 02:00:00"));
		when(dto.getIdProfissional()).thenReturn(1L);
		when(dto.getIdServico()).thenReturn(1L);
	}
	
	@Test
	public void geraAgendamentosSemConflito(){
		List<AgendamentoDTO> agendamentos = subject.gerar(dto);
		assertThat(agendamentos, hasSize(3));
		assertThat(agendamentos.get(0).getDataInicio(), is(parseData("31-08-1982 02:00:00")));
		assertThat(agendamentos.get(0).getDataFim(), is(parseData("31-08-1982 02:20:00")));
		assertThat(agendamentos.get(1).getDataInicio(), is(parseData("31-08-1982 02:20:00")));
		assertThat(agendamentos.get(1).getDataFim(), is(parseData("31-08-1982 02:40:00")));
		assertThat(agendamentos.get(2).getDataInicio(), is(parseData("31-08-1982 02:40:00")));
		assertThat(agendamentos.get(2).getDataFim(), is(parseData("31-08-1982 03:00:00")));
	}
	
	@Test
	public void geraAgendamentosComConflitoSimples(){
		when(agendamento.getDataInicio()).thenReturn(parseData("31-08-1982 02:20:00"));
		when(agendamento.getDataFim()).thenReturn(parseData("31-08-1982 02:40:00"));
		when(repositorioAgendamento.consulta(any(Profissional.class), any(Date.class))).thenReturn(newArrayList(agendamento));
		
		List<AgendamentoDTO> agendamentos = subject.gerar(dto);
		assertThat(agendamentos, hasSize(2));
		assertThat(agendamentos.get(0).getDataInicio(), is(parseData("31-08-1982 02:00:00")));
		assertThat(agendamentos.get(0).getDataFim(), is(parseData("31-08-1982 02:20:00")));
		assertThat(agendamentos.get(1).getDataInicio(), is(parseData("31-08-1982 02:40:00")));
		assertThat(agendamentos.get(1).getDataFim(), is(parseData("31-08-1982 03:00:00")));
	}
	
	@Test
	public void geraAgendamentosComConflitoComplexo(){
		when(agendamento.getDataInicio()).thenReturn(parseData("31-08-1982 02:20:00"));
		when(agendamento.getDataFim()).thenReturn(parseData("31-08-1982 02:45:00"));
		when(repositorioAgendamento.consulta(any(Profissional.class), any(Date.class))).thenReturn(newArrayList(agendamento));
		
		List<AgendamentoDTO> agendamentos = subject.gerar(dto);
		assertThat(agendamentos, hasSize(1));
		assertThat(agendamentos.get(0).getDataInicio(), is(parseData("31-08-1982 02:00:00")));
		assertThat(agendamentos.get(0).getDataFim(), is(parseData("31-08-1982 02:20:00")));
	}

	private void mockAtendimento() {
		when(intervalo.getDataInicio()).thenReturn(parseData("31-08-1982 02:00:00"));
		when(intervalo.getDataFim()).thenReturn(parseData("31-08-1982 03:00:00"));
		when(atendimento.getIntervalos()).thenReturn(newHashSet(intervalo));
		when(repositorioAtendimento.consulta(any(Profissional.class), anyInt())).thenReturn(atendimento);
	}

	private void mockProfissional() {
		when(repositorioProfissional.findById(anyLong())).thenReturn(profissional);
	}

	private void mockAgendamento() {
		when(repositorioAgendamento.consulta(any(Profissional.class), any(Date.class))).thenReturn(newArrayList());
	}

	private void mockServico() {
		when(servico.getTempoAtendimento()).thenReturn(TEMPO_ATENDIMENTO);
		when(servico.getPrestador()).thenReturn(prestador);
		when(repositorioServico.findById(anyLong())).thenReturn(servico);
	}

	private void mockSubject() {
		when(subject.getRepositorioAgendamento()).thenReturn(repositorioAgendamento);
		when(subject.getRepositorioAtendimento()).thenReturn(repositorioAtendimento);
		when(subject.getRepositorioProfissional()).thenReturn(repositorioProfissional);
		when(subject.getRepositorioServico()).thenReturn(repositorioServico);
	}
	
}
