package autochat.services.agendamento;

import static autochat.DateTestUtils.parseData;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import autochat.models.agendamento.Agendamento;

@RunWith(MockitoJUnitRunner.class)
public class BuscadorAgendamentoConflitanteTest {

	@Mock Agendamento agendamento;
	
	BuscadorAgendamentoConflitante subject = new BuscadorAgendamentoConflitante();
	
	@Before
	public void setUp(){
		when(agendamento.getDataInicio()).thenReturn(parseData("31-08-1982 01:40:00"));
		when(agendamento.getDataFim()).thenReturn(parseData("31-08-1982 02:00:00"));
	}
	
	@Test
	public void testaConflito1(){
		Date dataInicio = parseData("31-08-1982 01:30:00");
		Date dataFim = parseData("31-08-1982 01:50:00");
		
		Agendamento agendamentoEncontrado = subject.busca(Arrays.asList(agendamento), dataInicio, dataFim);
		assertThat(agendamentoEncontrado, is(agendamento));
	}
	
	@Test
	public void testaConflito2(){
		Date dataInicio = parseData("31-08-1982 01:50:00");
		Date dataFim = parseData("31-08-1982 02:10:00");
		
		Agendamento agendamentoEncontrado = subject.busca(Arrays.asList(agendamento), dataInicio, dataFim);
		assertThat(agendamentoEncontrado, is(agendamento));
	}
	
	@Test
	public void testaConflito3(){
		Date dataInicio = parseData("31-08-1982 01:40:00");
		Date dataFim = parseData("31-08-1982 02:00:00");
		
		Agendamento agendamentoEncontrado = subject.busca(Arrays.asList(agendamento), dataInicio, dataFim);
		assertThat(agendamentoEncontrado, is(agendamento));
	}
	
	@Test
	public void naoEnncontraConflito(){
		Date dataInicio = parseData("31-08-1982 02:00:00");
		Date dataFim = parseData("31-08-1982 02:20:00");
		
		Agendamento agendamentoEncontrado = subject.busca(Arrays.asList(agendamento), dataInicio, dataFim);
		assertThat(agendamentoEncontrado, nullValue());
	}
	
}
