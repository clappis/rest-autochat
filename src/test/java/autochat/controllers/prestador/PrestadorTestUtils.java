package autochat.controllers.prestador;

import autochat.services.prestador.PrestadorDTO;

public class PrestadorTestUtils {

	private static final long FACEBOOK_ID = 123456789321L;
	private static final String NOME = "Alan Clappis";
	private static final String CNPJ = "350886348460001";
	
	public static PrestadorDTO defaultDto() {
		PrestadorDTO dto = new PrestadorDTO();
		dto.setDocumento(CNPJ);
		dto.setNome(NOME);
		dto.setFacebookId(FACEBOOK_ID);
		return dto;
	}
	
}
