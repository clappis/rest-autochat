package autochat.controllers.prestador;

import static autochat.controllers.PostUtil.convertObjectToJsonBytes;
import static autochat.controllers.prestador.PrestadorTestUtils.defaultDto;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import autochat.ContextTestConfiguration;
import autochat.controllers.ControllerTest;
import autochat.services.prestador.PrestadorDTO;
import autochat.services.prestador.PrestadorService;

public class PrestadorControllerTest extends ControllerTest {

	@Autowired
	PrestadorService service; 
	
	@Configuration
    static class ContextConfiguration extends ContextTestConfiguration {
	
		@Bean
		public PrestadorService prestadorService(){
			return mock(PrestadorService.class);
		}
    }
	
	@Test
	public void consultaPrestadorHappyDay() throws Exception{
		getMockMvc().perform(get("/prestador/consulta"))
					.andExpect(status().isOk());
		
		verify(service, times(1)).consulta();
	}
	
	@Test
	public void consultaPrestadorPorFacebookId() throws Exception{
		getMockMvc().perform(get("/prestador/consultaPorFacebookId?facebookId=1"))
					.andExpect(status().isOk());
		
		verify(service, times(1)).consulta(1L);
	}
	
	@Test
	public void inserirPrestadorHappyDay() throws Exception{
		getMockMvc().perform(post("/prestador/inserir")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(convertObjectToJsonBytes(defaultDto())))
		.andExpect(MockMvcResultMatchers.status().isOk());

		verify(service, times(1)).inserir(any(PrestadorDTO.class));
		
	}
	
	@Test
	public void deletarPrestadorHappyDay() throws Exception{
		getMockMvc().perform(post("/prestador/deletar")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content("1")).andExpect(MockMvcResultMatchers.status().isOk());

		verify(service, times(1)).deletar(1L);
	}
}
