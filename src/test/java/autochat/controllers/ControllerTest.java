package autochat.controllers;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import autochat.RestAutochatApplication;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = RestAutochatApplication.class)
@WebAppConfiguration
public abstract class ControllerTest {

	private MockMvc mockMvc;
	
	@Autowired
	WebApplicationContext context;
	
	protected MockMvc getMockMvc() {
		return mockMvc;
	}

	@Before
	public void before(){
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}
}
