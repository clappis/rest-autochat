package autochat.controllers.agendamento;

import static autochat.DateTestUtils.parseData;
import static autochat.controllers.PostUtil.convertObjectToJsonBytes;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import autochat.ContextTestConfiguration;
import autochat.controllers.ControllerTest;
import autochat.services.agendamento.AgendamentoService;
import autochat.services.agendamento.ConsultaAgendamentoDto;
import autochat.services.agendamento.RequisicaoAgendamentoDto;

public class AgendamentoControllerTest extends ControllerTest {

	@Autowired
	AgendamentoService service; 
	
	@Configuration
    static class ContextConfiguration extends ContextTestConfiguration {
	
		@Bean
		public AgendamentoService agendamentoService(){
			return mock(AgendamentoService.class);
		}
    }
	
	@Test
	public void consultaAgendamentoHappyDay() throws Exception{
		getMockMvc().perform(get("/agendamento/consultaHorariosLivres/")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(convertObjectToJsonBytes(consultaDto())))
					.andExpect(status().isOk());
		
		verify(service, times(1)).consulta(any(ConsultaAgendamentoDto.class));
	}
	
	@Test
	public void agendarHappyDay() throws Exception{
		getMockMvc().perform(post("/agendamento/agendar")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(convertObjectToJsonBytes(requisicaoDto())))
		.andExpect(MockMvcResultMatchers.status().isOk());

		verify(service, times(1)).agendar(any(RequisicaoAgendamentoDto.class));
		
	}
	
	private ConsultaAgendamentoDto consultaDto() {
		ConsultaAgendamentoDto dto = new ConsultaAgendamentoDto();
		dto.setDia(parseData("31-08-1982 00:00:00"));
		dto.setIdProfissional(1L);
		dto.setIdServico(2L);
		return dto;
	}
	
	private RequisicaoAgendamentoDto requisicaoDto() {
		RequisicaoAgendamentoDto dto = new RequisicaoAgendamentoDto();
		dto.setDataInicio(parseData("31-08-1982 01:00:00"));
		dto.setDataFim(parseData("31-08-1982 01:30:00"));
		dto.setIdProfissional(1L);
		dto.setIdServico(2L);
		return dto;
	}

	
}
