package autochat.controllers.servico;

import static autochat.controllers.PostUtil.convertObjectToJsonBytes;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.Test;
import org.mockito.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import autochat.ContextTestConfiguration;
import autochat.controllers.ControllerTest;
import autochat.services.servico.ServicoDTO;
import autochat.services.servico.ServicoService;

public class ServicoControllerTest extends ControllerTest {

	@Autowired
	ServicoService service; 
	
	@Configuration
    static class ContextConfiguration extends ContextTestConfiguration {
	
		@Bean
		public ServicoService servicoService(){
			return mock(ServicoService.class);
		}
    }
	
	@Test
	public void consultaServicoHappyDay() throws Exception{
		getMockMvc().perform(MockMvcRequestBuilders.get("/servico/consulta"))
					.andExpect(MockMvcResultMatchers.status().isOk());
		
		verify(service, times(1)).consulta();
	}
	
	@Test
	public void deletarServicoHappyDay() throws Exception{
		getMockMvc().perform(post("/servico/deletar")
							.contentType(MediaType.APPLICATION_JSON_UTF8)
							.content("1")).andExpect(MockMvcResultMatchers.status().isOk());
		
		verify(service, times(1)).deletar(Matchers.anyLong());
	}
	
	@Test
	public void insereServicoHappyDay() throws Exception{
		getMockMvc().perform(post("/servico/inserir")
							.contentType(MediaType.APPLICATION_JSON_UTF8)
							.content(convertObjectToJsonBytes(defaultDto())))
					.andExpect(MockMvcResultMatchers.status().isOk());
		
		verify(service, times(1)).inserir(any(ServicoDTO.class));
	}
	
	private ServicoDTO defaultDto() {
		ServicoDTO dto = new ServicoDTO();
		dto.setNome("Nome");	
		dto.setTempoAtendimento(1);
		dto.setFacebookIdPrestador(1L);
		return dto;
	}
}
