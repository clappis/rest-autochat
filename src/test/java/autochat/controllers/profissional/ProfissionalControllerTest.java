package autochat.controllers.profissional;

import static autochat.controllers.PostUtil.convertObjectToJsonBytes;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import autochat.ContextTestConfiguration;
import autochat.controllers.ControllerTest;
import autochat.services.profissional.ProfissionalDTO;
import autochat.services.profissional.ProfissionalService;

public class ProfissionalControllerTest extends ControllerTest {

	private static final String NOME = "Alan Clappis";
	private static final String CPF = "35088634846";
	
	@Autowired
	ProfissionalService service; 
	
	@Configuration
    static class ContextConfiguration extends ContextTestConfiguration {
		
		@Bean
		public ProfissionalService profissionalService(){
			return mock(ProfissionalService.class);
		}
    }
	
	@Test
	public void consultaProfissionalHappyDay() throws Exception{
		getMockMvc().perform(get("/profissional/consulta"))
					.andExpect(status().isOk());
		
		verify(service).consulta();
	}
	
	@Test
	public void consultaProfissionalPorServico() throws Exception{
		getMockMvc().perform(get("/profissional/consultaPorServico?idServico=1"))
					.andExpect(status().isOk());
		
		verify(service, times(1)).consulta(1);
	}
	
	@Test
	public void inserirProfissionalHappyDay() throws Exception{
		getMockMvc().perform(post("/profissional/inserir")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(convertObjectToJsonBytes(defaultDto())))
		.andExpect(MockMvcResultMatchers.status().isOk());

		verify(service, times(1)).inserir(any(ProfissionalDTO.class));
		
	}
	
	@Test
	public void adicionarServicoHappyDay() throws Exception{
		getMockMvc().perform(post("/profissional/adicionarServico")
				.param("id", "1")
				.param("idServico", "2"))
				.andExpect(MockMvcResultMatchers.status().isOk());

		verify(service, times(1)).adicionaServico(1, 2);
	}

	private ProfissionalDTO defaultDto() {
		ProfissionalDTO dto = new ProfissionalDTO();
		dto.setCpf(CPF);
		dto.setNome(NOME);
		return dto;
	}
	
}
