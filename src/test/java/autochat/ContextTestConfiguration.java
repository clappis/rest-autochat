package autochat;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ContextTestConfiguration {

	@Bean
	public org.apache.tomcat.jdbc.pool.DataSource dataSource(){
		org.apache.tomcat.jdbc.pool.DataSource dataSource = new org.apache.tomcat.jdbc.pool.DataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost/autochat_teste");
        dataSource.setUsername("root");
        dataSource.setPassword("pma9rg15");
        return dataSource;
	}
}
